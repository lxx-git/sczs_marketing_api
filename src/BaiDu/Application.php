<?php

namespace Sczs\MarketingApi\BaiDu;

use Sczs\MarketingApi\BaiDu\Account\AccountServiceProvider;
use Sczs\MarketingApi\BaiDu\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\BaiDu\Material\MaterialServiceProvider;
use Sczs\MarketingApi\BaiDu\Campaign\CampaignServiceProvider;
use Sczs\MarketingApi\BaiDu\Group\GroupServiceProvider;
use Sczs\MarketingApi\BaiDu\Creative\CreativeServiceProvider;
use Sczs\MarketingApi\BaiDu\Kernel\ServiceContainer;
use Sczs\MarketingApi\BaiDu\Report\ReportServiceProvider;
use Sczs\MarketingApi\BaiDu\Tool\ToolServiceProvider;
use Sczs\MarketingApi\BaiDu\Auth\AccessTokenServiceProvider;

//use Sczs\MarketingApi\BaiDu\Material\MaterialServiceProvider;
//use Sczs\MarketingApi\BaiDu\Plan\PlanServiceProvider;
//use Sczs\MarketingApi\BaiDu\Report\ReportServiceProvider;


/**
 * Class Application
 * @property \Sczs\MarketingApi\BaiDu\Account\Advertiser $advertiser
 * @property \Sczs\MarketingApi\BaiDu\Tool\Tool $tool
 * @property \Sczs\MarketingApi\BaiDu\Campaign\Campaign $campaign
 * @property \Sczs\MarketingApi\BaiDu\Group\Group $group
 * @property \Sczs\MarketingApi\BaiDu\Creative\Creative $creative
 * @property \Sczs\MarketingApi\BaiDu\Report\Report $report
// * @property \Sczs\MarketingApi\BaiDu\Material\Material $material
 * @property \Sczs\MarketingApi\BaiDu\Kernel\Cache\Cache $cache
 * @property \Sczs\MarketingApi\BaiDu\Material\Video $video
 * @property \Sczs\MarketingApi\BaiDu\Material\Image $image
 * @property \Sczs\MarketingApi\BaiDu\Auth\AccessToken $oauth
 * @package Sczs\MarketingApi\BaiDu
 */
class Application extends ServiceContainer
{

    protected $providers = [
        AccessTokenServiceProvider::class,
        AccountServiceProvider::class,
        ToolServiceProvider::class,
        CampaignServiceProvider::class,
        GroupServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
        CacheServiceProvider::class
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }

}