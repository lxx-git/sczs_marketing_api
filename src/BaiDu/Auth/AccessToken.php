<?php


namespace Sczs\MarketingApi\BaiDu\Auth;

use Sczs\MarketingApi\BaiDu\Kernel\AccessToken as BaseAccessToken;
use Sczs\MarketingApi\BaiDu\Kernel\ServiceContainer;

class AccessToken extends BaseAccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }

}