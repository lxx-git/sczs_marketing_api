<?php

namespace Sczs\MarketingApi\BaiDu\Campaign;

use Sczs\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Campaign extends BaseHttpClient
{
    /**获取广告计划信息
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException
     */
    public function lists(array $data)
    {
        return $this->httpJsonPost('/CampaignFeedService/getCampaignFeed',$data);
    }

    /**创建广告计划
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(array $data){

        return $this->httpJsonPost('/CampaignFeedService/addCampaignFeed',$data);
    }

}