<?php

namespace Sczs\MarketingApi\BaiDu\Kernel;

use Sczs\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\BaiDu\Kernel\Traits\HasHttpRequest;

class AccessToken implements AccessTokenInterface
{

    use HasHttpRequest;

    /**
     * @var \Sczs\MarketingApi\Kuaishou\Kernel\ServiceContainer;
     */
    protected $app;

    protected $storeAccessTokenKey = 'sczs:baidu:access_token_';

    protected $oauth2 = 'https://u.baidu.com/oauth/accessToken';

    protected $refreshTokenPath = 'https://u.baidu.com/oauth/refreshToken';

    protected $managerId = "";

    public function __construct($app)
    {
        $this->app = $app;
    }


    public function authCode2ToKen()
    {
        $res = $this->request("POST",$this->oauth2,["json" => [
            'appId' => $this->app['config']->get('appId'),
            'secretKey' => $this->app['config']->get('secretKey'),
            'authCode' => $this->app['config']->get('authCode'),
            'grantType' => 'auth_code',
            'userId' => $this->app['config']->get('userId'),
        ]]);
        $this->managerId = $this->app['config']->get("manager_id");

        $content = $res->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setAccessToken($decode['data']);

        return $decode['data'];
    }

    public function refreshToken()
    {
        $tokenInfo = $this->request("POST",$this->refreshTokenPath,["json" => [
            'appId' => $this->app['config']->get('appId'),
            'secretKey' => $this->app['config']->get('secretKey'),
            'refreshToken' => $this->app['config']->get("refreshToken"),
            'userId' => $this->app['config']->get("userId"),
        ]]);
        $this->managerId = $this->app['config']->get("manager_id");

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setAccessToken($decode['data']);

        return $decode;
    }

    public function setAccessToken($data)
    {
        $data["request_expires_in"] = time();
        $this->getCache()->hmSet($this->storeAccessTokenKey . $this->managerId, $data);
        $this->getCache()->setExp($this->storeAccessTokenKey . $this->managerId, 86400*30);
    }


    /**
     * @return \Sczs\MarketingApi\Kuaishou\Kernel\Cache\Cache
     */
    public function getCache()
    {
        return $this->app['cache'];
    }


    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->app['config']->get('access_token');
    }

}