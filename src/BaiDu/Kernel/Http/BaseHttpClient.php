<?php
namespace Sczs\MarketingApi\BaiDu\Kernel\Http;


use Sczs\MarketingApi\Helper\Arr;
use Sczs\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException;
use Sczs\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\BaiDu\Kernel\Traits\HasHttpRequest;
use Sczs\MarketingApi\BaiDu\Kernel\Traits\RequestSandbox;
use function Couchbase\defaultDecoder;

class BaseHttpClient
{

    use HasHttpRequest {
        request as perRequest;
    }
    use RequestSandbox;

    /**
     * @var \Sczs\MarketingApi\BaiDu\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * @var \Sczs\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface
     */
    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ]
    ];

    public function __construct($app)
    {
        $this->app = $app;
        $type = $this->app['config']->get('type');

        if($type == "sms"){
            $this->useSandbox(false);
        }
    }


    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $url, array $options = [])
    {

        $base_url = $this->getHost();

        $url = $base_url.$url;

        $res = $this->perRequest($method,$url,$options);

        $content =  json_decode($res->getBody()->getContents(),320);

        if($content["header"]["status"] !== 0) throw new \Exception(json_encode($content['header']['failures'],JSON_UNESCAPED_UNICODE));

        return $content['body'];
    }


    public function validateRequiredParams(array $data,array $keys)
    {
        if ( !Arr::keys_all_exists($data,$keys) ) throw new ValidateRequestParamException('required keys :'.implode(',',$keys));
    }



}