<?php


namespace Sczs\MarketingApi\BaiDu\Kernel\Interfaces;


interface AccessTokenInterface
{

    public function getToken();

}