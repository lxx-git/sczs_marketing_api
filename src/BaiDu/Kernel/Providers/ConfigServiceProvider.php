<?php

namespace Sczs\MarketingApi\BaiDu\Kernel\Providers;

use Sczs\MarketingApi\BaiDu\Kernel\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['config']) && $pimple['config'] = function ($app){
            return new Config($app->getConfig());
        };
    }
}