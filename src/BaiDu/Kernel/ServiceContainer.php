<?php

namespace Sczs\MarketingApi\BaiDu\Kernel;

use Sczs\MarketingApi\BaiDu\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\BaiDu\Group\GroupServiceProvider;
use Sczs\MarketingApi\BaiDu\Kernel\Providers\ConfigServiceProvider;
use Sczs\MarketingApi\BaiDu\Kernel\Providers\HttpClientServiceProvider;
//use Sczs\MarketingApi\BaiDu\Kernel\Providers\RequestServiceProvider;
use Pimple\Container;

class ServiceContainer extends Container
{

    protected $defaultConfig = [];

    protected $userConfig = [];

    protected $providers = [];

    public function __construct($config = [],array $values = [])
    {
        $this->userConfig = $config;

        parent::__construct($values);

        $this->registerProviders($this->getProviders());
    }

    public function getConfig() : array
    {

        $base = [
            'http' => [
                'timeout' => 60,
                'base_uri' => 'https://api.baidu.com/json/feed/',
            ],

        ];

        return array_replace_recursive($base,$this->defaultConfig,$this->userConfig);
    }

    public function getProviders() : array
    {
        return array_merge([
            ConfigServiceProvider::class,
            HttpClientServiceProvider::class,
            CacheServiceProvider::class,
        ],$this->providers);
    }


    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}