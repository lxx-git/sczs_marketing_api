<?php


namespace Sczs\MarketingApi\BaiDu\Kernel\Traits;


trait RequestSandbox
{

    protected $sandbox = true;

    public function useSandbox($sandbox=true) : self
    {
        $this->sandbox = $sandbox;
        return $this;
    }

    public function isSandbox() : bool
    {
        return $this->sandbox;
    }

    public function getHost()
    {
        return $this->isSandbox() ? $this->getSandBoxHost() : $this->getProductHost();
    }

    public function getOption($options){

       return $this->app['config']->get('type') == 'video' ? $options : ["header" => $this->app['config']->get('header'),"body" => $options];
    }
    public function getSandBoxHost()
    {
        return 'https://api.baidu.com/json/feed/';
    }

    public function getProductHost()
    {
        return 'https://api.baidu.com/json/sms/service/';
    }

}