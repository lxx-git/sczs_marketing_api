<?php


namespace Sczs\MarketingApi\BaiDu\Material;

use Sczs\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;
class video extends BaseHttpClient
{

    /**上传视频
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadVideo(array $data)
    {

        return $this->multipart("/VideoUploadService/addVideo",$data);
    }

}