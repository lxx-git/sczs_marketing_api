<?php

namespace Sczs\MarketingApi\BaiDu\Report;

use Sczs\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * 推广报告
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException
     */
    public function ReportData(array $data)
    {
        return $this->httpJsonPost('OpenApiReportService/getReportData',$data);
    }

    /**
     * 获取账户信息
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userInfo(array $data){

        return $this->httpJsonPost('/AccountFeedService/getAccountFeed',$data);
    }

    /**
     * 获取app列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
}