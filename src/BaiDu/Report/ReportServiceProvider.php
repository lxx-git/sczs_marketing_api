<?php


namespace Sczs\MarketingApi\BaiDu\Report;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ReportServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        !isset($pimple['report']) && $pimple['report'] = function ($app){
            return new Report($app);
        };
    }
}