<?php


namespace Sczs\MarketingApi\BaiDu\Tool;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ToolServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        !isset($pimple['tool']) && $pimple['tool'] = function ($app){
            return new Tool($app);
        };
    }
}