<?php

namespace Sczs\MarketingApi;


use Sczs\MarketingApi\Kuaishou\Application as Kuaishou;
use Sczs\MarketingApi\BaiDu\Application as Baidu;
use Sczs\MarketingApi\Tencent\Application as Tencent;
use Sczs\MarketingApi\Toutiao\Application as Toutiao;
use Sczs\MarketingApi\Vivo\Application as Vivo;
use Sczs\MarketingApi\Huawei\Application as Huawei;
use Sczs\MarketingApi\Oppo\Application as Oppo;
use Sczs\MarketingApi\FeiShu\Application as Feishu;

class Factory
{


    /**
     * 快手
     * @param array $config
     * @return Kuaishou
     */
    public static function Kuaishou(array $config)
    {

        return new Kuaishou($config);
    }

    /**
     * 广点通，腾讯
     * @param array $config
     * @return Tencent
     */
    public static function Tencent(array $config)
    {
        return new Tencent($config);
    }

    /**
     * 巨量引擎 ，头条
     * @param array $config
     * @return Toutiao
     */
    public static function oceanEngine(array $config)
    {
        return new Toutiao($config);
    }

    /** 百度
     * @param array $config
     * @return Baidu
     */
    public static function BaiDu(array $config)
    {
        return new Baidu($config);
    }

    /**
     *  vivo
     * @param array $config
     * @return Vivo
     */
    public static function Vivo(array $config)
    {
        return new Vivo($config);
    }

    /**
     * huawei
     * @param array $config
     * @return Huawei
     */
    public static function Huawei(array $config)
    {
        return new Huawei($config);
    }

    /**
     * oppo
     * @param array $config
     * @return Oppo
     */
    public static function Oppo(array $config)
    {
        return new Oppo($config);
    }

    /**
     * 飞书
     * @param array $config
     * @return Feishu
     */
    public static function Feishu(array $config)
    {
        return new Feishu($config);
    }

}