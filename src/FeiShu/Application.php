<?php

namespace Sczs\MarketingApi\FeiShu;

use Sczs\MarketingApi\FeiShu\Kernel\ServiceContainer;
use Sczs\MarketingApi\FeiShu\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\FeiShu\Message\MessageServiceProvider;
use Sczs\MarketingApi\FeiShu\Oauth\OauthServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\FeiShu\Oauth\Oauth $oauth
 * @property \Sczs\MarketingApi\FeiShu\Message\Message $message
 * @property \Sczs\MarketingApi\FeiShu\Kernel\Cache\Cache $cache
 * @package Sczs\MarketingApi\Vivo
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        OauthServiceProvider::class,
        CacheServiceProvider::class,
        MessageServiceProvider::class
    ];
}
