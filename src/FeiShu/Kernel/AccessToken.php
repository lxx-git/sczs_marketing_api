<?php

namespace Sczs\MarketingApi\FeiShu\Kernel;

use Sczs\MarketingApi\FeiShu\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\FeiShu\Kernel\Traits\HttpRequest;

class AccessToken implements AccessTokenInterface
{
    use HttpRequest {
        request as FeiShuRequest;
    }

    protected $app;

    protected $LoginUrl = "https://open.feishu.cn/open-apis/authen/v1/access_token";

    protected $oauthTokenUrl = "https://open.feishu.cn/open-apis/auth/v3/app_access_token/internal";

//    protected $authorizeCodeKey = "sczs:feishu:authorize_code";

    protected $authorizeAccessTokenKey = "sczs:feishu:access_token_";

    protected $authorizeRefreshTokenKey = "sczs:feishu:refresh_token_";

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * 飞书登陆
     * @throws Exception\FeiShuException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $code
     * @return mixed
     */
    public function authorization($code)
    {
        $parameters = [
            'app_id' => $this->app->defaultConfig['app_id'],
            'app_secret' => $this->app->defaultConfig['app_secret'],
            'grant_type' => "authorization_code",
            'code' => $code,
        ];

        $token = $this->access_token();

        return $this->FeiShuRequest("POST", $this->LoginUrl, [
            "json" => $parameters,
            'headers' => [
                'Authorization' => "Bearer " . $token['app_access_token']
            ]
        ]);
    }

    /**
     * 飞书刷新token
     * @throws Exception\FeiShuException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return mixed
     */
    public function refreshToken()
    {
        $parameters = [
            'app_id' => $this->app->defaultConfig['app_id'],
            'app_secret' => $this->app->defaultConfig['app_secret']
        ];

        $response = $this->FeiShuRequest("POST", $this->oauthTokenUrl, ['json'=>$parameters]);

        $this->setAccessToken($response);

        return $response;
    }


    public function setAccessToken($data)
    {
        $this->getCacheClient()->hmSet(
            $this->authorizeAccessTokenKey . $this->app->defaultConfig['app_id'], $data
        );
        $this->getCacheClient()->setExp($this->authorizeAccessTokenKey . $this->app->defaultConfig['app_id'], 86400 * 359);
    }

    //只有一个应用 直接获取
    public function access_token()
    {
        return $this->getCacheClient()->hgetall(
            $this->authorizeAccessTokenKey . $this->app->defaultConfig['app_id']
        );
    }

    /**
     * @return \Sczs\MarketingApi\FeiShu\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app['cache'];
    }

    public function getToken()
    {
        // TODO: Implement getToken() method.
    }
}