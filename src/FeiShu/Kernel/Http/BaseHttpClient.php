<?php

namespace Sczs\MarketingApi\FeiShu\Kernel\Http;

use Sczs\MarketingApi\FeiShu\Kernel\Exception\VivoException;
use Sczs\MarketingApi\FeiShu\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as FeiShuRequest;
    }

    /**
     * @var \Sczs\MarketingApi\FeiShu\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://open.feishu.cn/open-apis/'
        ]
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @throws VivoException
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->owner = $app->defaultConfig;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\FeiShu\Kernel\Exception\FeiShuException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        $interface = $this->defaults['http']['base_uri'] . $url;
        return $this->FeiShuRequest($method, $interface, [
            "json" => $parameters,
            "headers" => [
                "Content-Type"=> "application/json",
                "Authorization"=> "Bearer ". $this->owner["tenant_access_token"]
            ]
        ]);
    }
}