<?php

namespace Sczs\MarketingApi\FeiShu\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();

    public function refreshToken();
}