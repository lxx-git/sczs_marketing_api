<?php

namespace Sczs\MarketingApi\FeiShu\Kernel\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Sczs\MarketingApi\FeiShu\Kernel\Exception\FeiShuException;

/**
 * Trait HttpRequest
 *
 * @package Sczs\MarketingApi\Tencent\Traits
 */
trait HttpRequest
{

    /**
     * @throws GuzzleException
     * @throws FeiShuException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function request(string $method, string $url, array $parameters = [])
    {

        $ret = $this->httpClient()->request(
            $method,
            $url,
            $parameters
        );

        if(!$ret) throw new GuzzleException(json_encode(["msg" => "Feishu returned an error:", "code" => "-1"]));

        $content = json_decode($ret->getBody()->getContents(), 320);

        if (!$content) throw new FeiShuException(json_encode(["msg" => "Feishu return empty content", "code" => "-1"]));

        if ($content["code"] !== 0) throw new FeiShuException(json_encode($content, 320));

        if (isset($content["data"]))
            return $content["data"];
        else
            return $content;

    }

    /**
     * 实例化请求
     * @return Client
     */
    public function httpClient()
    {
        return new Client();
    }
}
