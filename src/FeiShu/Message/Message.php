<?php

namespace Sczs\MarketingApi\FeiShu\Message;

use Sczs\MarketingApi\FeiShu\Kernel\Http\BaseHttpClient;

class Message extends BaseHttpClient
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\FeiShu\Kernel\Exception\FeiShuException
     * @param $data
     * @return mixed
     */
    public function send_message($data)
    {
        return $this->request('POST', 'message/v4/send/', $data);
    }
}
