<?php

namespace Sczs\MarketingApi\FeiShu\Message;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class MessageServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['message'] = function ($app) {
            return new Message($app);
        };
    }
}
