<?php

namespace Sczs\MarketingApi\FeiShu\Oauth;

use Sczs\MarketingApi\FeiShu\Kernel\AccessToken;
use Sczs\MarketingApi\FeiShu\Kernel\ServiceContainer;

class Oauth extends AccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }
}