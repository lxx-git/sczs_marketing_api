<?php

namespace Sczs\MarketingApi\FeiShu\Oauth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class OauthServiceProvider
 *
 * @author lixiangxu
 *
 */
class OauthServiceProvider implements ServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['oauth'] = function ($app) {
            return new Oauth($app);
        };
    }
}
