<?php

namespace Sczs\MarketingApi\Huawei;

use Sczs\MarketingApi\Huawei\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Huawei\Kernel\Provider\ConfigServiceProvider;
use Sczs\MarketingApi\Huawei\Kernel\ServiceContainer;
use Sczs\MarketingApi\Huawei\Oauth\OauthServiceProvider;
use Sczs\MarketingApi\Huawei\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\Huawei\Report\Report $report
 * @property \Sczs\MarketingApi\Huawei\Oauth\Oauth $oauth
 * @property \Sczs\MarketingApi\Huawei\Kernel\Cache\Cache $cache
 * @package Sczs\MarketingApi\Vivo
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        ReportServiceProvider::class,
        OauthServiceProvider::class,
        CacheServiceProvider::class,
        ConfigServiceProvider::class
    ];
}
