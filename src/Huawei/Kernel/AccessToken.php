<?php

namespace Sczs\MarketingApi\Huawei\Kernel;

use Sczs\MarketingApi\Huawei\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Huawei\Kernel\Traits\HttpRequest;

class AccessToken implements AccessTokenInterface
{
    use HttpRequest {
        request as HWRequest;
    }

    protected $app;

    protected $authCodeTokenPath = 'https://login.cloud.huawei.com/oauth2/v2/token';//获取token

    protected $refreshTokenPath = 'https://login.cloud.huawei.com/oauth2/v2/token';//刷新token

    protected $authorizeAccessTokenKey = "sczs:huawei:access_token_";

    protected $authorizeRefreshTokenKey = "sczs:huawei:refresh_token_";

    protected $managerId = "";

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @throws Exception\HuaweiException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @return mixed
     */
    public function authCodeToken()
    {
        $parameters = [
            'grant_type' => $this->app->defaultConfig["grant_type"],
            'code' => $this->app->defaultConfig['authorization_code'],
            'client_id' => $this->app->defaultConfig['client_id'],
            'client_secret' => $this->app->defaultConfig['client_secret'],
            'redirect_uri' => $this->app->defaultConfig['redirect_uri'],
        ];

        $this->managerId = $this->app->defaultConfig['advertiser_id'];

        $content = $this->HWRequest('POST', $this->authCodeTokenPath, ['form_params' => $parameters]);

        $this->setAccessToken($content);

        return $content;
    }

    /**
     * @throws Exception\HuaweiException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $config
     * @return mixed
     */
    public function refreshToken($config)
    {
        $parameters = [
            'grant_type' => $this->app->defaultConfig["grant_type"],
            'refresh_token' => $config['refresh_token'],
            'client_id' => $this->app->defaultConfig['client_id'],
            'client_secret' => $this->app->defaultConfig['client_secret']
        ];

        $this->managerId = $config['advertiser_id'];

        $content = $this->HWRequest('POST', $this->authCodeTokenPath, ['form_params' => $parameters]);

        $this->setAccessToken($content);

        return $content;
    }

    public function setAccessToken($data)
    {
        $data["request_expires_in"] = time();
        $data["advertiser_id"] = $this->managerId;
        $this->getCacheClient()->hmSet($this->authorizeAccessTokenKey . $this->managerId, $data);
        $this->getCacheClient()->setExp($this->authorizeAccessTokenKey . $this->managerId, 3600);
    }

    public function access_token($managerId)
    {
        return $this->getCacheClient()->hgetall(
            $this->authorizeAccessTokenKey . $managerId
        );
    }

    /**
     * @return \Sczs\MarketingApi\Huawei\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app['cache'];
    }

    public function getToken()
    {
        // TODO: Implement getToken() method.
    }

    public function refToken()
    {
        // TODO: Implement refToken() method.
    }
}
