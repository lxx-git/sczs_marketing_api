<?php

namespace Sczs\MarketingApi\Huawei\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();

    public function refToken();
}
