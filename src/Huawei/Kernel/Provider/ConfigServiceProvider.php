<?php


namespace Sczs\MarketingApi\Huawei\Kernel\Provider;


use Sczs\MarketingApi\Huawei\Kernel\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['config']) && $pimple['config'] = function ($app){
            return new Config($app->getConfig());
        };
    }
}