<?php

namespace Sczs\MarketingApi\Huawei\Oauth;

use Sczs\MarketingApi\Huawei\Kernel\AccessToken;
use Sczs\MarketingApi\Huawei\Kernel\ServiceContainer;

class Oauth extends AccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }
}