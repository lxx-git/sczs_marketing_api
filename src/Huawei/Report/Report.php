<?php

namespace Sczs\MarketingApi\Huawei\Report;

use Sczs\MarketingApi\Huawei\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{
    /**
     * huawei广告计划
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Huawei\Kernel\Exception\HuaweiException
     * @param array $parameters
     * @return array|string
     */
    public function campaign(array $parameters = [])
    {
        return $this->request("POST", "reports/campaign/query", $parameters);
    }
}