<?php

namespace Sczs\MarketingApi\Kuaishou;

use Sczs\MarketingApi\Kuaishou\Account\AccountServiceProvider;
use Sczs\MarketingApi\Kuaishou\Auth\AccessTokenServiceProvider;
use Sczs\MarketingApi\Kuaishou\Campaign\CampaignServiceProvider;
use Sczs\MarketingApi\Kuaishou\Creative\CreativeServiceProvider;
use Sczs\MarketingApi\Kuaishou\Kernel\ServiceContainer;
use Sczs\MarketingApi\Kuaishou\Material\MaterialServiceProvider;
use Sczs\MarketingApi\Kuaishou\Plan\PlanServiceProvider;
use Sczs\MarketingApi\Kuaishou\Report\ReportServiceProvider;


/**
 * Class Application
 * @property \Sczs\MarketingApi\Kuaishou\Account\Advertiser $advertiser
 * @property \Sczs\MarketingApi\Kuaishou\Auth\AccessToken $access_token
 * @property \Sczs\MarketingApi\Kuaishou\Kernel\Cache\Cache $cache
 * @property \Sczs\MarketingApi\Kuaishou\Campaign\Campaign $campaign
 * @property \Sczs\MarketingApi\Kuaishou\Creative\Creative $creative
 * @property \Sczs\MarketingApi\Kuaishou\Creative\Program $program
 * @property \Sczs\MarketingApi\Kuaishou\Plan\Plan $plan
 * @property \Sczs\MarketingApi\Kuaishou\Report\Report $report
 * @property \Sczs\MarketingApi\Kuaishou\Material\Material $material
 * @property \Sczs\MarketingApi\Kuaishou\Material\Video $video
 * @property \Sczs\MarketingApi\Kuaishou\Material\Image $image
 * @package Sczs\MarketingApi\Kuaishou
 */
class Application extends ServiceContainer
{

    protected $providers = [
        AccessTokenServiceProvider::class,
        AccountServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }

}