<?php

namespace Sczs\MarketingApi\Kuaishou\Auth;

use Sczs\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface;

use Sczs\MarketingApi\Kuaishou\Kernel\AccessToken as BaseAccessToken;

class AccessToken extends BaseAccessToken
{


    /**
     * @param string $redirectUri 授权回调地址
     * @param string $state 原样返回的字符
     * @param array|string[] $scope  ["ad_query","report_service","account_service","ad_manage","account_cert"] account_cert是代理商才有的
     * @param bool $agent 是否在代理商进行授权
     * @return string
     */
    public function authWebUrl(
        string $redirectUri,
        string $state,
        array $scope=["ad_query","report_service","account_service","ad_manage"],
        bool $agent=false) : string
    {

        $query = [
            'app_id' => $this->app['config']->get('app_id'),
            'scope' => urlencode(json_encode($scope)),
            'redirect_uri' => $redirectUri,
            'state' => $state
        ];

        if ( $agent ) $query['oauth_type'] = 'agent';

        return $this->getProductHost() . 'openapi/oauth?'.http_build_query($query);
    }


    /**
     * @param string $redirectUri
     * @param string $state
     * @param array|string[] $scope
     * @return string
     */
    public function compassAuthWebUrl(string $redirectUri,string $state,array $scope=["ad_query","report_service","account_service","ad_manage"]) : string
    {
        return 'https://developers.e.kuaishou.com/tools/authorize?'.http_build_query([
                'app_id' => $this->app['config']->get('app_id'),
                'scope' => urlencode(json_encode($scope)),
                'state' => $state,
                'redirect_uri' => $redirectUri,
            ]);
    }

}