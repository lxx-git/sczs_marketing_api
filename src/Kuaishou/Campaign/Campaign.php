<?php

namespace Sczs\MarketingApi\Kuaishou\Campaign;

use Sczs\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Campaign extends BaseHttpClient
{

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.1.1
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/campaign/list',$data);
    }

    /**
     * 创建
     * @param array $data
     * <p>advertiser_id int </p>
     * <p>campaign_name string </p>
     * <p>type int </p>
     * <p>sub_type int </p>
     * <p>day_budget int option</p>
     * <p>day_budget_schedule[] int </p>
     * @return array
     */
    public function create(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_name',
            'type',
            'sub_type',
            'day_budget_schedule',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/campaign/create',$data);
    }

    /**
     * 修改广告计划
     * @param array $data
     * <p>advertiser_id int </p>
     * <p>campaign_id string </p>
     * <p>campaign_name string </p>
     * <p>day_budget int option</p>
     * <p>day_budget_schedule int[] </p>
     * @return array
     * @see https://developers.e.kuaishou.com/docs/3.3.2
     */
    public function update(array $data) :array
    {

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/campaign/update',$data);
    }

    /**
     * 修改广告计划预算
     * @param array $data
     * <p>advertiser_id int </p>
     * <p>campaign_id string </p>
     * <p>day_budget int option</p>
     * <p>day_budget_schedule int[] </p>
     * @return array
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.3.3
     */
    public function updateBudget(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/campaign/update',$data);
    }

    /**
     * 修改广告计划状态
     * @param array $data
     * <p>advertiser_id int </p>
     * <p>campaign_id int </p>
     * <p>campaign_ids int[] </p>
     * <p>put_status int[] </p>
     * @return array
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.3.4
     */
    public function updateStatus(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
            'put_status',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/campaign/update/status',$data);
    }




}