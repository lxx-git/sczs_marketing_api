<?php

namespace Sczs\MarketingApi\Kuaishou\Creative;

use Sczs\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Creative extends BaseHttpClient
{

    /**
     * 获取广告创意信息
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.1.3
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/creative/list',$data);
    }

    /**
     * 创建创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.1
     */
    public function create(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'creative_name',
            'creative_material_type',
            'action_bar_text',
            'description',
            'creative_tag',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/creative/create',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.4
     */
    public function update(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'creative_id',
            'creative_tag',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/creative/update',$data);
    }

    /**
     * @param array $data
     * <p>creative_ids int[] options</p>
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.6
     */
    public function updateStatus(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'creative_id',
            'put_status',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/creative/update/status',$data);
    }

    /**
     * 预览，体验
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.7
     */
    public function preview(array $data):array
    {

        if ( !key_exists('unit_type',$data) ) $data['unit_type'] = 4;

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'user_ids',
            'unit_type',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/creative/preview',$data);
    }

    /**
     * 建议
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.8
     */
    public function advise(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/creative/creative_tag/advise',$data);
    }
}