<?php

namespace Sczs\MarketingApi\Kuaishou\Creative;

use Sczs\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Program extends BaseHttpClient
{

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.1.4
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/creative/advanced/program/list',$data);
    }

    /**
     * 程序化2.0创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.2
     */
    public function create(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'package_name',
            'horizontal_photo_ids',
            'vertical_photo_ids',
            'cover_image_tokens',
            'action_bar',
            'captions',
            'creative_tag',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/creative/advanced/program/create',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.5
     */
    public function update(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'package_name',
            'horizontal_photo_ids',
            'vertical_photo_ids',
            'cover_image_tokens',
            'action_bar',
            'captions',
            'creative_tag',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/creative/advanced/program/create',$data);
    }

    /**
     * 预览，体验
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.5.7
     */
    public function preview(array $data) : array
    {
        if ( !key_exists('unit_type',$data) ) $data['unit_type'] = 7;

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'user_ids',
            'unit_type',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/creative/preview',$data);
    }

    /**
     * 获取程序化创意2.0审核信息
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.1.5
     */
    public function reviewList(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'user_ids',
            'unit_type',
        ]);

        return $this->httpJsonGet('rest/openapi/v2/creative/advanced/program/review_detail',$data);
    }
}