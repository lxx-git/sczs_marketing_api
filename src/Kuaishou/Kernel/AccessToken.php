<?php

namespace Sczs\MarketingApi\Kuaishou\Kernel;

use Sczs\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Kuaishou\Kernel\Traits\HasHttpRequest;
use Sczs\MarketingApi\Kuaishou\Kernel\Traits\RequestSandbox;

class AccessToken implements AccessTokenInterface
{

    use HasHttpRequest;
    use RequestSandbox;


    /**
     * @var \Sczs\MarketingApi\Kuaishou\Kernel\ServiceContainer;
     */
    protected $app;

    protected $storeAccessTokenKey = 'sczs:kuaishou:access_token:';

    protected $storeRefreshTokenKey = 'sczs:kuaishou:refresh_token:';

    protected $cacheAdvertiserIdPrefix = 'sczs:kuaishou:advertiser_id:';

    protected $cacheAdvertiserIdsPrefix = 'sczs:kuaishou:advertiser_ids:';

    protected $authCode2TokenPath = 'rest/openapi/oauth2/authorize/access_token';

    protected $refreshTokenPath = 'rest/openapi/oauth2/authorize/refresh_token';

    protected $managerId = "";

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function getAdvertiserId() : string
    {

        $cacheKey = $this->getCacheAdvertiserIdKey();
        $cache = $this->getCache();
        if ( $cache->has($cacheKey) && $res = $cache->get($cacheKey) ) return $res;

        return '';
    }

    /**
     * @return string
     */
    public function getCacheAdvertiserIdKey() : string
    {
        return $this->cacheAdvertiserIdPrefix . $this->app['config']->get('manager_id');
    }

    public function getCacheAdvertiserIdsKey() : string
    {
        return $this->cacheAdvertiserIdsPrefix . $this->app['config']->get('manager_id');
    }

    public function getAdvertiserIds() : array
    {

        $cacheKey = $this->getCacheAdvertiserIdsKey();
        $cache = $this->getCache();

        if ( $cache->has($cacheKey) && $ids = $cache->SMEMBERS($cacheKey) ) return $ids;

        return [];
    }

    public function getToken($refresh=false)
    {
        $cache = $this->getCache();
        $cacheKey = $this->getStoreTokenKey();

        if ( $refresh === false ){
            if ( $cache->has($cacheKey) && $res = $cache->get( $cacheKey ) )
                return $res;
        }

        //是否有refresh token
        $refreshToken = $this->getRefreshToken();

        if ( !$refreshToken ) throw new \Exception('no refresh token,please try to get auth code');

        $data = $this->refreshToken2AccessToken($refreshToken);

        return $data['access_token'];
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $authCode
     * @return mixed
     */
    public function authCode2ToKen($authCode)
    {
        $res = $this->httpJsonPost($this->authCode2TokenPath,[
            'app_id' => $this->app['config']->get('app_id'),
            'secret' => $this->app['config']->get('secret'),
            'auth_code' => $authCode,
        ]);

        $this->managerId = $this->app['config']->get('manager_id');

        $content = $res->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setAccessToken($decode['data']);

        return $decode['data'];
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $config
     * @return mixed
     */
    public function refreshToken($config)
    {
        $tokenInfo = $this->httpJsonPost($this->refreshTokenPath,[
            'app_id' => $config["app_id"],
            'secret' => $config["secret"],
            'refresh_token' => $config["refresh_token"],
        ]);

        $this->managerId = $config["manager_id"];

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setAccessToken($decode["data"]);

        return $decode;
    }

    /**
     * @return \Sczs\MarketingApi\Kuaishou\Kernel\Cache\Cache
     */
    public function getCache()
    {
        return $this->app['cache'];
    }

    public function setToken($token,$lifetime=3600)
    {
        $this->getCache()->set($this->getStoreTokenKey(),$token,$lifetime);
    }

    public function setRefreshToken($token,$lifetime=86400)
    {
        $this->getCache()->set($this->getRefreshTokenKey(),$token,$lifetime);
    }

    public function getRefreshToken()
    {
        $cache = $this->getCache();
        $key = $this->getRefreshTokenKey();

        return $cache->has($key) ? $cache->get($key) : null;
    }

    public function setAccessToken(array $data)
    {
//        $this->setToken($data['access_token'],$data['access_token_expires_in']);
        $data["request_expires_in"] = time();
        $this->getCacheClient()->hmSet($this->storeAccessTokenKey . $this->managerId, $data);
        $this->getCacheClient()->setExp($this->storeAccessTokenKey . $this->managerId, 86400*30);
        $this->setAdvertiserIds($data["advertiser_ids"]);
    }

    public function access_token($managerId)
    {
        return $this->getCacheClient()->hgetall(
            $this->storeAccessTokenKey . $managerId
        );
    }

    /**
     * @return \Sczs\MarketingApi\Kuaishou\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app["cache"];
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function setAdvertiserIds(array $ids) : self
    {
        $cacheKey = $this->getCacheAdvertiserIdsKey();
        $cache = $this->getCache();

        $cache->SAdd($cacheKey,...$ids);

        return $this;
    }

    public function refreshToken2AccessToken($refreshToken='')
    {
        if ( empty($refreshToken) ){
             $refreshToken = $this->getRefreshToken();
        }

        if ( !$refreshToken ) throw new \Exception('no refresh token, please try to get auth code');

        $tokenInfo = $this->httpJsonPost($this->refreshTokenPath,[
            'app_id' => $this->app['config']->get('app_id'),
            'secret' => $this->app['config']->get('secret'),
            'refresh_token' => $refreshToken,
        ]);

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setTokenInfo($decode['data']);

        return $decode['data'];
    }

    public function refresh(): AccessTokenInterface
    {

        $this->getToken(true);

        return $this;
    }

    public function getStoreTokenKey() : string
    {
        return $this->storeAccessTokenKey . ':'. $this->app['config']->get('app_id') ;
    }

    public function getRefreshTokenKey() : string
    {
        return $this->storeRefreshTokenKey . ':'. $this->app['config']->get('app_id') ;
    }

}