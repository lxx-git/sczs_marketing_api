<?php
namespace Sczs\MarketingApi\Kuaishou\Kernel\Cache;

class Cache
{

    /**
     * @var mixed 只要能调用redis命令的
     */
    protected $driver;

    public function set($key,$value,$exp)
    {
        $this->driver->setex($key,$exp,$value);
    }

    public function get($key)
    {
        return $this->driver->get($key);
    }

    public function forever($key,$value)
    {
        $this->driver->set($key,$value);
    }

    /**
     * @param mixed $driver
     */
    public function setDriver($driver): void
    {
        $this->driver = $driver;
    }

    public function has($key)
    {
        return $this->driver->exists($key);
    }

    public function __call($name,$params)
    {
        return $this->driver->{$name}(...$params);
    }

    public function setExp(string $key, int $exp)
    {
        $this->driver->Expire($key, $exp);
    }

    /**
     * hash
     * 获取哈希表所有字段
     * @param string $key
     * @return bool
     */
    public function hgetall(string $key)
    {
        if (empty($key)) {
            return false;
        }

        return $this->driver->hgetall($key);
    }

    /**
     * hash多字段新增
     * @param string $key
     * @param array $array
     * @return bool
     */
    public function hmSet(string $key, array $array)
    {
        if (!is_array($array)) {
            return false;
        }

        return $this->driver->hmset($key, $array);
    }

}