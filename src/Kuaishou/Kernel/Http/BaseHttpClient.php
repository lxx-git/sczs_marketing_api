<?php

namespace Sczs\MarketingApi\Kuaishou\Kernel\Http;


use Sczs\MarketingApi\Helper\Arr;
use Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException;
use Sczs\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Kuaishou\Kernel\Traits\HasHttpRequest;
use Sczs\MarketingApi\Kuaishou\Kernel\Traits\RequestSandbox;

class BaseHttpClient
{

    use HasHttpRequest {
        request as perRequest;
    }
    use RequestSandbox;

    /**
     * @var \Sczs\MarketingApi\Kuaishou\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * @var \Sczs\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface
     */
    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://ad.e.kuaishou.com'
        ]
    ];

    public function __construct($app, AccessTokenInterface $accessToken = null)
    {
        $this->app = $app;

        $this->useSandbox($this->app['config']->get('sandbox', false));
    }


    public function getAccessToken()
    {
        return $this->accessToken;
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $url
     * @param array $options
     * @param $method
     * @return array
     */
    public function request($method, $url, array $options = [])
    {
        $options["advertiser_id"] = $this->app['config']->get('advertiser_id');

        $url = $this->verificationRequestUri($method, $options, $url);

        $parameters = $this->verificationRequestParameters($method, $options);

        $res = $this->perRequest($method, $url, $parameters);

        $content = json_decode($res->getBody()->getContents(), true);

        if (!$content || $content['code'] != 0) throw new \Exception(json_encode($content, JSON_UNESCAPED_UNICODE));

        return $content['data'];
    }

    public function verificationRequestUri($method, $parameters, $url)
    {
        $httpBasicInfo = self::$defaults['http'];

        $url = $httpBasicInfo["base_uri"] . "/" . $url;

        if (strtoupper($method) === "GET") {
            $url .= "?" . http_build_query($parameters);
        }

        return $url;
    }

    public function verificationRequestParameters($method, $parameters)
    {

        self::$defaults["headers"]['Access-Token'] = $this->app['config']->get('access_token');

        $parameters['base_uri'] = $this->getHost();

        //如果是post, 确保 GuzzleHttp 请求参数
        if ($method === "POST") {
            $parameters = ["json" => $parameters];
        }

        return array_merge(self::$defaults, $parameters);
    }

    public function validateRequiredParams(array $data, array $keys)
    {
        if (!Arr::keys_all_exists($data, $keys)) throw new ValidateRequestParamException('required keys :' . implode(',', $keys));
    }


}