<?php


namespace Sczs\MarketingApi\Kuaishou\Kernel\Interfaces;


interface AccessTokenInterface
{

    public function getToken();

    public function refresh() : self;

}