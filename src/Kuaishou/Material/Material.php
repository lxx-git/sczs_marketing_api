<?php

namespace Sczs\MarketingApi\Kuaishou\Material;

use Sczs\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Material extends BaseHttpClient
{

    /**
     * 素材报表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/4.7
     */
    public function lists(array $data) : array
    {

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'view_type',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/material_report',$data);

    }
}