<?php

namespace Sczs\MarketingApi\Kuaishou\Report;

use Sczs\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{

    /**
     * 代理商数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function agent(array $data): array
    {
        $this->validateRequiredParams($data, [
            'agent_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/agent/report', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * <p>temporal_granularity enum("DAILY","HOURLY") default DAILY </p>
     * <p>start_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>end_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>start_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>end_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>report_dims string[] "adScene"：按广告场景；不传/传空/传空数组：不限 </p>
     * @return array
     * @see https://developers.e.kuaishou.com/docs/4.2
     */
    public function advertiser(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/account_report', $data);
    }

    /**
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $data
     * @return array
     * <p>temporal_granularity enum("DAILY","HOURLY") default DAILY </p>
     * <p>start_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>end_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>start_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>end_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>campaign_ids int[] 可选，单次查询数量不超过5000 </p>
     * <p>report_dims string[] "adScene"：按广告场景；不传/传空/传空数组：不限 </p>
     */
    public function campaign(array $data): array
    {
//        $this->validateRequiredParams($data, [
//            'advertiser_id',
//        ]);

        return $this->request("POST", "rest/openapi/v1/report/campaign_report", $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * <p>temporal_granularity enum("DAILY","HOURLY") default DAILY </p>
     * <p>start_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>end_date_min Y-m-d H:i required if temporal_granularity = HOURLY</p>
     * <p>start_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>end_date Y-m-d required if temporal_granularity = DAILY</p>
     * <p>campaign_ids int[] 可选，单次查询数量不超过5000 </p>
     * <p>unit_ids int[] 可选，单次查询数量不超过5000 </p>
     * <p>report_dims string[] "adScene"：按广告场景；不传/传空/传空数组：不限 </p>
     * @return array
     * @see https://developers.e.kuaishou.com/docs/4.3
     */
    public function plan(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/campaign_report', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     * @see https://developers.e.kuaishou.com/docs/4.5
     */
    public function creative(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/creative_report', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     * @see https://developers.e.kuaishou.com/docs/4.6
     */
    public function program(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/program_creative_report', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     * @see https://developers.e.kuaishou.com/docs/4.7
     */
    public function material(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
            'view_type',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/report/program_creative_report', $data);
    }

}