<?php

namespace Sczs\MarketingApi\Oppo\Activation;

use Sczs\MarketingApi\Oppo\Kernel\Http\BaseHttpClient;

class Activation extends BaseHttpClient
{

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Oppo\Kernel\Exception\OppoException
     * @param array $parameters
     * @return mixed
     */
    public function activation(array $parameters = [])
    {
        return $this->activationRequest("POST", "clue/sendData", $parameters);
    }

}
