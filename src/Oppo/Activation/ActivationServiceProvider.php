<?php

namespace Sczs\MarketingApi\Oppo\Activation;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ActivationServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["activation"] = function ($app) {
            return new Activation($app);
        };
    }
}