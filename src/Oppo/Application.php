<?php

namespace Sczs\MarketingApi\Oppo;

use Sczs\MarketingApi\Oppo\Activation\ActivationServiceProvider;
use Sczs\MarketingApi\Oppo\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Oppo\Kernel\ServiceContainer;
use Sczs\MarketingApi\Oppo\Oauth\OauthServiceProvider;
use Sczs\MarketingApi\Oppo\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\Oppo\Report\Report $report
 * @property \Sczs\MarketingApi\Oppo\Oauth\Oauth $oauth
 * @property \Sczs\MarketingApi\Oppo\Activation\Activation $activation
 * @property \Sczs\MarketingApi\Oppo\Kernel\Cache\Cache $cache
 * @package Sczs\MarketingApi\Oppo
 */
class Application extends ServiceContainer
{
    protected $providers = [
        ReportServiceProvider::class,
        OauthServiceProvider::class,
        CacheServiceProvider::class,
        ActivationServiceProvider::class,
    ];
}
