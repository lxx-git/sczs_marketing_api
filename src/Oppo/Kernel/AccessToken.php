<?php

namespace Sczs\MarketingApi\Oppo\Kernel;

use Sczs\MarketingApi\Oppo\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Oppo\Kernel\Traits\HttpRequest;

class AccessToken implements AccessTokenInterface
{
    use HttpRequest {
        request as OppoRequest;
    }

    protected $app;

    protected $authorizeAccessTokenKey = "sczs:oppo:access_token_";

    protected $authorizeRefreshTokenKey = "sczs:oppo:refresh_token_";

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function access_token($ownerId)
    {
        return $this->getCacheClient()->hgetall($this->authorizeAccessTokenKey . $ownerId);
    }

    /**
     * @return \Sczs\MarketingApi\Oppo\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app['cache'];
    }

    public function getToken()
    {
        // TODO: Implement getToken() method.
    }

    public function refToken()
    {
        // TODO: Implement refToken() method.
    }
}
