<?php

namespace Sczs\MarketingApi\Oppo\Kernel\Http;

use Sczs\MarketingApi\Oppo\Kernel\Exception\OppoException;
use Sczs\MarketingApi\Oppo\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as OppoRequest;
        RequestActivation as RequestActivation;
    }

    /**
     * @var \Sczs\MarketingApi\Oppo\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://sapi.ads.oppomobile.com/v3/'
        ],
        'http_v1' => [
            'timeout' => 10,
            'base_uri' => 'https://sapi.ads.oppomobile.com/v1/'
        ],
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @throws OppoException
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        $defaultConfig = $app->defaultConfig;

        $advertiser_id = $app->defaultConfig["advertiser_id"];

        $owner_info = $app->oauth->access_token($advertiser_id);

        if(!$owner_info) throw new OppoException("此账号没有授权");

        $this->owner = [
            "advertiser_id" => $defaultConfig["advertiser_id"],
            "api_id" => $owner_info["api_id"],
            "api_key" => $owner_info["api_key"],
        ];

    }

    /**
     * @throws OppoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        //oppo加密参数
        $sign = $this->sign($this->owner);

        return $this->OppoRequest($method, $url, [
            "form_params" => $parameters,
            'headers' => [
                'Authorization' => "Bearer " . $sign
            ]
        ]);
    }

    /**
     * @throws OppoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function activationRequest(string $method, string $url, array $parameters = [])
    {
        //oppo加密参数
        $sign = $this->sign($this->owner);

        return $this->RequestActivation($method, $url, [
            "json" => $parameters,
            'headers' => [
                'Authorization' => "Bearer " . $sign
            ]
        ]);
    }

    /**
     * oppo header
     * @param $owner
     * @return string Authorization: Bearer
     */
    public function sign($owner)
    {
        $time = time();

        $sign = sha1($owner["api_id"] . $owner["api_key"] . $time);

        return base64_encode($owner["advertiser_id"] . "," . $owner["api_id"] . "," . $time . "," . $sign);
    }

    /**
     * 多账号
     * 传入账号的所有参数
     * @param array $config
     */
    public function setDefaultConfig(array $config)
    {
        $this->owner = [
            "api_id" => $config["api_id"],
            "api_key" => $config["api_key"],
            "advertiser_id" => $config["advertiser_id"],
        ];
    }
}