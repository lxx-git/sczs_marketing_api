<?php

namespace Sczs\MarketingApi\Oppo\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();

    public function refToken();
}