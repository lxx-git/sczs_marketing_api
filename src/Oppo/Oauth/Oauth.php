<?php

namespace Sczs\MarketingApi\Oppo\Oauth;

use Sczs\MarketingApi\Oppo\Kernel\AccessToken;
use Sczs\MarketingApi\Oppo\Kernel\ServiceContainer;

class Oauth extends AccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }

}
