<?php

namespace Sczs\MarketingApi\Oppo\Oauth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class OauthServiceProvider implements ServiceProviderInterface
{
    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['oauth'] = function ($app) {
            return new Oauth($app);
        };
    }
}
