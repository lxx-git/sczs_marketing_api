<?php

namespace Sczs\MarketingApi\Tencent\Advertiser;

use Sczs\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Advertiser extends BaseHttpClient
{

    /**
     * 添加广告主账号
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function advertiser_get(array $parameters = [])
    {
        return $this->request("GET", "advertiser/get", $parameters);
    }

}