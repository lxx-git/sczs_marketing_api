<?php

namespace Sczs\MarketingApi\Tencent\Advertiser;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ReportServiceProvider
 * @package Sczs\MarketingApi\Tencent\Report
 */
class AdvertiserServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["advertiser"] = function ($app) {
            return new Advertiser($app);
        };
    }
}