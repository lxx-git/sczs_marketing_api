<?php

namespace Sczs\MarketingApi\Tencent;

use Sczs\MarketingApi\Tencent\Ad\AdServiceProvider;
use Sczs\MarketingApi\Tencent\Advertiser\AdvertiserServiceProvider;
use Sczs\MarketingApi\Tencent\Campaign\CampaignServiceProvider;
use Sczs\MarketingApi\Tencent\Creative\CreativeServiceProvider;
use Sczs\MarketingApi\Tencent\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Tencent\Kernel\ServiceContainer;
use Sczs\MarketingApi\Tencent\Material\MaterialServiceProvider;
use Sczs\MarketingApi\Tencent\Oauth\OauthServiceProvider;
use Sczs\MarketingApi\Tencent\Pages\PagesServiceProvider;
use Sczs\MarketingApi\Tencent\Plan\PlanServiceProvider;
use Sczs\MarketingApi\Tencent\ProductCatalogs\ProductCatalogsProvider;
use Sczs\MarketingApi\Tencent\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\Tencent\Oauth\Oauth $oauth
 * @property \Sczs\MarketingApi\Tencent\Advertiser\Advertiser $advertiser
 * @property \Sczs\MarketingApi\Tencent\Campaign\Campaign $campaign
 * @property \Sczs\MarketingApi\Tencent\Plan\Plan $pan
 * @property \Sczs\MarketingApi\Tencent\Creative\Creative $creative
 * @property \Sczs\MarketingApi\Tencent\Ad\Ad $ad
 * @property \Sczs\MarketingApi\Tencent\Report\Report $report
 * @property \Sczs\MarketingApi\Tencent\Material\Brand $brand
 * @property \Sczs\MarketingApi\Tencent\Material\Image $image
 * @property \Sczs\MarketingApi\Tencent\Material\Video $video
 * @property \Sczs\MarketingApi\Tencent\ProductCatalogs\ProductCatalogs $product_catalogs
 * @property \Sczs\MarketingApi\Tencent\Pages\Pages $page
 * @property \Sczs\MarketingApi\Tencent\Kernel\Cache\Cache $cache
 * @package Sczs\MarketingApi\Tencent
 */

class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        OauthServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        AdvertiserServiceProvider::class,
        MaterialServiceProvider::class,
        CacheServiceProvider::class,
        PagesServiceProvider::class,
        ProductCatalogsProvider::class,
        ReportServiceProvider::class,
    ];
}
