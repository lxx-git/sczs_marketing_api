<?php

namespace Sczs\MarketingApi\Tencent\Campaign;

use Sczs\MarketingApi\Tencent\CommonActions\Operate;

class Campaign extends Operate
{
    protected $interface = "campaigns";
}