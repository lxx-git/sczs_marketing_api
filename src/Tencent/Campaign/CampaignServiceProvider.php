<?php

namespace Sczs\MarketingApi\Tencent\Campaign;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class CampaignServiceProvider
 * @package Sczs\MarketingApi\Tencent\Campaign
 */
class CampaignServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['campaign'] = function ($app) {
            return new Campaign($app);
        };
    }
}