<?php

namespace Sczs\MarketingApi\Tencent\CommonActions;

use Sczs\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Operate extends BaseHttpClient
{
    protected $interface;

    /**
     * 新建
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function create(array $parameters = [])
    {
        return $this->request("POST", "{$this->interface}/add", $parameters);
    }

    /**
     * 修改
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function update(array $parameters = [])
    {
        return $this->request("POST","{$this->interface}/update", $parameters);
    }

    /**
     * 删除
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function delete(array $parameters = [])
    {
        return $this->request("POST", "{$this->interface}/delete", $parameters);
    }
}