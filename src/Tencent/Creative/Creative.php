<?php

namespace Sczs\MarketingApi\Tencent\Creative;

use Sczs\MarketingApi\Tencent\CommonActions\Operate;

class Creative extends Operate
{
    protected $interface = "adcreatives";
}