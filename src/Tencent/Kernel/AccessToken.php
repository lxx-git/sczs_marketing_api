<?php

namespace Sczs\MarketingApi\Tencent\Kernel;

use Sczs\MarketingApi\Tencent\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Tencent\Kernel\Traits\HttpRequest;

class AccessToken implements AccessTokenInterface
{
    use HttpRequest {
        request as TencentRequest;
    }

    protected $app;

    protected $authorizeUrl = "https://api.e.qq.com/oauth/authorize";

    protected $authorizeTokenUrl = "https://api.e.qq.com/oauth/token";

    protected $authorizeCodeKey = "sczs:gdt:authorize_code";

    protected $authorizeAccessToken = "sczs:gdt:access_token_";

    protected $authorizeRefreshToken = "sczs:gdt:refresh_token_";

    protected $callBackUrl = "https://system.sczsie.com/api/auth/ad/vivoAuthCallback";
    public $managerId = "";

    //authorization_code,state
    //client_id,client_secret,grant_type,authorization_code
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * 获取授权token
     * @param array $param
     * @return mixed
     * @throws Exception\TencentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAuthToken(array $param)
    {
       $AccessToken = $this->request("GET",$this->authorizeTokenUrl,
       [
           "client_id" => $param['client_id'],
           "client_secret" => $param['client_secret'],
           "grant_type" => "authorization_code",
           "authorization_code" => $param['authorization_code'],
           "redirect_uri" => $this->callBackUrl,
       ]);
        $this->managerId = $this->app['config']->get("manager_id");

       $this->setAccessToken($AccessToken);
       return $AccessToken;
    }


    public function refreshToken($param)
    {
        $AccessToken = $this->request("GET",$this->authorizeTokenUrl,
            [
                "client_id" => $param['client_id'],
                "client_secret" => $param['client_secret'],
                "grant_type" => "refresh_token",
                "refresh_token" => $param['refresh_token'],
            ]);

        $this->managerId = $this->app['config']->get("manager_id");

        $this->setAccessToken($AccessToken);
        return $AccessToken;
    }


    public function setAccessToken($data)
    {
        $data["request_expires_in"] = time();
        $this->getCacheClient()->hmSet($this->authorizeAccessToken . $this->managerId, $data);
        $this->getCacheClient()->setExp($this->authorizeAccessToken . $this->managerId, 86400*30);
    }

    /**
     * @param $ownerId
     * @return bool
     */
    public function access_token($ownerId)
    {
        return $this->getCacheClient()->hgetall($this->authorizeAccessToken.$ownerId);

    }

    /**
     * @return \Sczs\MarketingApi\Tencent\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app['cache'];
    }


    public function getToken()
    {
        // TODO: Implement getToken() method.
        return $this->app['config']->get('access_token');
    }


}