<?php

namespace Sczs\MarketingApi\Tencent\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();

}