<?php

namespace Sczs\MarketingApi\Tencent\Kernel;

use Pimple\Container;

class ServiceContainer extends Container
{
    /**
     * 默认配置
     * @var array
     */
    public $defaultConfig = [];

    /**
     * provider类
     * @var array
     */
    protected $providers = [

    ];

    /**
     * 实例化注册
     * ServiceContainer constructor.
     * @param array $config
     * @param array $values
     */
    public function __construct($config = [],array $values = [])
    {
        $this->defaultConfig = $config;

        parent::__construct($values);

        $this->registerProviders($this->getProviders());
    }

    public function getConfig() : array
    {

        $base = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'http' => [
                'timeout' => 10,
                'base_uri' => 'https://api.e.qq.com/v1.3/'
            ]
        ];

        return array_replace_recursive($base,$this->defaultConfig,$this->config);
    }
    /**
     * 获取注册类数组
     * @return array
     */
    public function getProviders() : array
    {
        return array_merge([],$this->providers);
    }

    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }


    public function __get($name)
    {
        return $this->offsetGet($name);
    }
}