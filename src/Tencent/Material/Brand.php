<?php

namespace Sczs\MarketingApi\Tencent\Material;

use Sczs\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Brand extends BaseHttpClient
{
    /**
     * 上传品牌图片
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function upload(array $parameters = [])
    {
        return $this->multipartRequest($this->verificationRequestUri("POST", $parameters, "brand/add"), $parameters);
    }

    /**
     * 品牌图片列表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get(array $parameters = [])
    {
        return $this->request("GET", "brand/get", $parameters);
    }
}
