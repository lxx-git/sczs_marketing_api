<?php

namespace Sczs\MarketingApi\Tencent\Oauth;

use Sczs\MarketingApi\Tencent\Kernel\AccessToken;
use Sczs\MarketingApi\Tencent\Kernel\ServiceContainer;

class Oauth extends AccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }
}