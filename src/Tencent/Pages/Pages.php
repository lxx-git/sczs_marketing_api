<?php

namespace Sczs\MarketingApi\Tencent\Pages;

use Sczs\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Pages extends BaseHttpClient
{
    /**
     * 落地页列表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get(array $parameters = [])
    {
        return $this->request("GET", "pages/get", $parameters);
    }
}