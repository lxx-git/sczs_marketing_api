<?php

namespace Sczs\MarketingApi\Tencent\Report;

use Sczs\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{

    /**
     * 广点通数据报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function daily_reports(array $parameters = [])
    {
        return $this->request("GET", "daily_reports/get", $parameters);
    }

    /**
     * 广点通广告计划报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return array|string
     */
    public function plan(array $parameters = [])
    {
        return $this->request("GET", "campaigns/get", $parameters);
    }

    /**
     * 广点通广告组报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param $parameters
     * @return array|string
     */
    public function campaign(array $parameters = [])
    {
        return $this->request("GET", "adgroups/get", $parameters);
    }

    /**
     * 广点通广告报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param $parameters
     * @return array|string
     */
    public function ad(array $parameters = [])
    {
        return $this->request("GET", "ads/get", $parameters);
    }
}