<?php


namespace Sczs\MarketingApi\Toutiao\Account;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Advertiser extends BaseHttpClient
{

    /**
     * 查询账号余额
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function fund($data)
    {
        return $this->request('GET', 'open_api/2/advertiser/fund/get/', $data);
    }

    /**
     *查询账号当日的资金流水
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function dailyStat($data)
    {
        return $this->request('GET', 'open_api/2/advertiser/fund/daily_stat/', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function majordomo($data)
    {
        return $this->request('GET', 'open_api/2/majordomo/advertiser/select/', $data);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $data
     * @return mixed
     */
    public function integrated($data)
    {
        return $this->request('GET', 'open_api/2/report/integrated/get/', $data);
    }
}