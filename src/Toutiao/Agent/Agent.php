<?php

namespace Sczs\MarketingApi\Toutiao\Agent;

use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Agent extends BaseHttpClient
{
    /**
     * 代理商报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function report(array $data)
    {
        $this->validateRequiredParams($data,[
            'agent_id', 'start_date', 'end_date'
        ]);

        return $this->httpJsonGet('open_api/2/report/agent/get_v2/',$data);
    }

    /**
     * 代理商信息
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function agentInfo(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_ids'
        ]);

        return $this->httpJsonGet('open_api/2/agent/info/',$data);
    }

    /**
     * 二级代理商列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function childAgent(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);

        return $this->httpJsonGet('open_api/2/agent/child_agent/select/',$data);
    }

    /**
     * 获取代理商
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function agentList(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);

        return $this->httpJsonGet('open_api/2/agent/advertiser/select/',$data);
    }

}
