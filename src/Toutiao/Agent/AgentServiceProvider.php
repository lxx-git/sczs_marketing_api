<?php

namespace Sczs\MarketingApi\Toutiao\Agent;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class AgentServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['agent']) && $pimple['agent'] = function ($app){
            return new Agent($app);
        };
    }
}