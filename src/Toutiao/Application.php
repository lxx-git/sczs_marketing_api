<?php


namespace Sczs\MarketingApi\Toutiao;


use Sczs\MarketingApi\Toutiao\Account\AccountServiceProvider;
use Sczs\MarketingApi\Toutiao\Agent\AgentServiceProvider;
use Sczs\MarketingApi\Toutiao\Auth\AccessTokenServiceProvider;
use Sczs\MarketingApi\Toutiao\Campaign\CampaignServiceProvider;
use Sczs\MarketingApi\Toutiao\Creative\CreativeServiceProvider;
use Sczs\MarketingApi\Toutiao\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Toutiao\Kernel\ServiceContainer;
use Sczs\MarketingApi\Toutiao\Material\MaterialServiceProvider;
use Sczs\MarketingApi\Toutiao\Plan\PlanServiceProvider;
use Sczs\MarketingApi\Toutiao\Project\ProjectServiceProvider;
use Sczs\MarketingApi\Toutiao\Promotion\PromotionServiceProvider;
use Sczs\MarketingApi\Toutiao\Report\ReportServiceProvider;
use Sczs\MarketingApi\Toutiao\Tools\ToolsServiceProvider;
use Sczs\MarketingApi\Toutiao\Async\AsyncServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\Toutiao\Account\Advertiser $advertiser
 * @property  \Sczs\MarketingApi\Toutiao\Auth\AccessToken $oauth
 * @property \Sczs\MarketingApi\Toutiao\Kernel\Cache\Cache $cache
 * @property \Sczs\MarketingApi\Toutiao\Campaign\Campaign $campaign
 * @property \Sczs\MarketingApi\Toutiao\Plan\Plan $plan
 * @property \Sczs\MarketingApi\Toutiao\Creative\Creative $creative
 * @property \Sczs\MarketingApi\Toutiao\Report\Report $report
 * @property \Sczs\MarketingApi\Toutiao\Async\Async $async
 * @property \Sczs\MarketingApi\Toutiao\Material\Material $material
 * @property \Sczs\MarketingApi\Toutiao\Material\Image $image
 * @property \Sczs\MarketingApi\Toutiao\Material\Video $video
 * @property \Sczs\MarketingApi\Toutiao\Tools\Tools $tools
 * @property \Sczs\MarketingApi\Toutiao\Agent\Agent $agent
 * @property \Sczs\MarketingApi\Toutiao\Project\Project $project
 * @property \Sczs\MarketingApi\Toutiao\Promotion\Promotion $promotion
 * @package Sczs\MarketingApi\Toutiao
 */
class Application extends ServiceContainer
{
    protected $providers = [
        AccessTokenServiceProvider::class,
        AccountServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
        ToolsServiceProvider::class,
        CacheServiceProvider::class,
        AgentServiceProvider::class,
        AsyncServiceProvider::class,
        ProjectServiceProvider::class,
        PromotionServiceProvider::class,
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }
}