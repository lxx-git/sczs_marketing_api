<?php


namespace Sczs\MarketingApi\Toutiao\Async;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Async extends BaseHttpClient
{

    /**
     * 创建异步任务
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $this->validateRequiredParams($data, [
            'task_name',
            'force'
        ]);
        return $this->request("POST", 'open_api/2/async_task/create/', $data);
    }

    /**
     * 获取异步任务列表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function asyncTask(array $data): array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id'
        ]);
        return $this->request("GET", 'open_api/2/async_task/get/', $data);
    }

//    /**
//     * 下载任务结果 返回结果不是json
//     * @throws \GuzzleHttp\Exception\GuzzleException
//     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
//     * @param array $data
//     * @return array
//     */
//    public function download(array $data): array
//    {
//        $this->validateRequiredParams($data, [
//            'advertiser_id',
//            'task_id'
//        ]);
//        return $this->request("GET", 'open_api/2/async_task/download/', $data);
//    }

}