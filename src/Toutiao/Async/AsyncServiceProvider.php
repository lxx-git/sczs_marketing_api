<?php


namespace Sczs\MarketingApi\Toutiao\Async;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class AsyncServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['async']) && $pimple['async'] = function ($app){
            return new Async($app);
        };
    }
}