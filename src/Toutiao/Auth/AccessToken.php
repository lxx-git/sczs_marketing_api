<?php


namespace Sczs\MarketingApi\Toutiao\Auth;

use Sczs\MarketingApi\Toutiao\Kernel\AccessToken as BaseAccessToken;
use Sczs\MarketingApi\Toutiao\Kernel\ServiceContainer;

class AccessToken extends BaseAccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }

}