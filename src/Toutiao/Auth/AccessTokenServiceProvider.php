<?php


namespace Sczs\MarketingApi\Toutiao\Auth;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class AccessTokenServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $pimple
     */
    public function register(Container $pimple)
    {
        !isset($pimple['oauth']) && $pimple['oauth'] = function ($app){
            return new AccessToken($app);
        };
    }
}