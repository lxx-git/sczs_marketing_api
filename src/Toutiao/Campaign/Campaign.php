<?php


namespace Sczs\MarketingApi\Toutiao\Campaign;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Campaign extends BaseHttpClient
{
    /**
     * 获取广告组列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonGet('open_api/2/campaign/get/',$data);
    }

    /**
     * 创建广告组
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function create(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'campaign_name',
            'budget_mode',
            'landing_type'
        ]);

        return $this->httpJsonPost('open_api/2/campaign/create/',$data);
    }

    /**
     * 修改广告组
     * @param array $data
     * @return array
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function update(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
            'modify_time'
        ]);

        return $this->httpJsonPost('open_api/2/campaign/update/',$data);
    }

    /**
     * 修改广告组状态
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateStatus(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_ids',
            'opt_status'
        ]);

        return $this->httpJsonPost('open_api/2/campaign/update/status/',$data);
    }


    /**
     * 获取管家投放项目列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function groups(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonGet('open_api/2/adlab/groups/get/',$data);
    }


    /**
     * 获取管家投放项目素材信息
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function groupMaterialInfo(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'group_id',
        ]);

        return $this->httpJsonGet('open_api/2/adlab/group_material_info/get/',$data);
    }
}