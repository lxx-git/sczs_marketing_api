<?php


namespace Sczs\MarketingApi\Toutiao\Campaign;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class CampaignServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['campaign']) && $pimple['campaign'] = function ($app){
            return new Campaign($app);
        };
    }
}