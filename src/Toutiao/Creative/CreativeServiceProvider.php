<?php


namespace Sczs\MarketingApi\Toutiao\Creative;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class CreativeServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['creative']) && $pimple['creative'] = function ($app){
            return new Creative($app);
        };
    }
}