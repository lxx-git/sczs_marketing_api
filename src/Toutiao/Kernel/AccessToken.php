<?php


namespace Sczs\MarketingApi\Toutiao\Kernel;

use Sczs\MarketingApi\Toutiao\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Toutiao\Kernel\Traits\HasHttpRequest;

class AccessToken implements AccessTokenInterface
{

    use HasHttpRequest;

    protected $app;


    protected $authCodeTokenPath = 'open_api/oauth2/access_token/';//获取token

    protected $refreshTokenPath = 'open_api/oauth2/refresh_token/';//刷新token

    protected $authorizeAccessTokenKey = "sczs:tt:access_token_";

    protected $authorizeRefreshTokenKey = "sczs:tt:refresh_token_";

    protected $managerId = "";

    public function __construct($app)
    {
        $this->app = $app;

    }

    public function getToken()
    {
        return $this->app['config']->get('access_token');
    }

    /**
     * 授权
     * @param $name
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authCodeToken($name)
    {
        $res = $this->httpJsonPost($this->authCodeTokenPath,[
            'app_id' => $this->app['config']->get('app_id'),
            'secret' => $this->app['config']->get('secret'),
            'grant_type' => 'auth_code',
            'auth_code' => $this->app['config']->get('auth_code'),
        ]);

        $this->managerId = $this->app['config']->get("manager_id");

        $content = $res->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $decode["data"]['advertiser_ids'] = json_encode($decode["data"]['advertiser_ids']);
        $decode['data']['manager_name'] = $name;
        $this->setAccessToken($decode["data"]);

        return $decode;
    }

    /**
     * 刷新token
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $config
     * @return mixed
     */
    public function refreshToken($config)
    {
        $tokenInfo = $this->httpJsonPost($this->refreshTokenPath,[
            'app_id' => $config["app_id"],
            'secret' => $config["secret"],
            'grant_type' => 'refresh_token',
            'refresh_token' => $config["refresh_token"],
        ]);

        $this->managerId = $config["manager_id"];

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setAccessToken($decode["data"]);

        return $decode;
    }

    public function setAccessToken($data)
    {
        $data["request_expires_in"] = time();
        $this->getCacheClient()->hmSet($this->authorizeAccessTokenKey . $this->managerId, $data);
        $this->getCacheClient()->setExp($this->authorizeAccessTokenKey . $this->managerId, 86400*30);
    }

    public function access_token($managerId)
    {
        return $this->getCacheClient()->hgetall(
            $this->authorizeAccessTokenKey . $managerId
        );
    }

    /**
     * @return \Sczs\MarketingApi\Toutiao\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app["cache"];
    }

}