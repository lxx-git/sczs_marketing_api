<?php


namespace Sczs\MarketingApi\Toutiao\Kernel\Cache;


use Sczs\MarketingApi\Toutiao\Kernel\Cache\Cache;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
class CacheServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['cache']) && $pimple['cache'] = function (){
            return new Cache();
        };
    }
}