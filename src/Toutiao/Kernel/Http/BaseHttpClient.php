<?php


namespace Sczs\MarketingApi\Toutiao\Kernel\Http;


use Illuminate\Support\Facades\Log;
use Sczs\MarketingApi\Helper\Arr;
use Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException;
use Sczs\MarketingApi\Toutiao\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Toutiao\Kernel\Traits\HasHttpRequest;

class BaseHttpClient
{
    use HasHttpRequest {
        request as perRequest;
    }

    /**
     * @var \Sczs\MarketingApi\Toutiao\Kernel\ServiceContainer
     */
    protected $app;

    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://ad.oceanengine.com'
        ]
    ];

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $url
     * @param array $options
     * @param $method
     * @return mixed
     */
    public function request($method, $url, array $options = [])
    {
     //   $options["advertiser_id"] = $this->app['config']->get('advertiser_id');

        $base_url = $url;
        $url = $this->verificationRequestUri($method, $options, $base_url);

        $parameters = $this->verificationRequestParameters($method, $options);

        $res = $this->perRequest($method, $url, $parameters);

        $content = json_decode($res->getBody()->getContents(), true);

        if (!$content || $content['code'] != 0){

            Log::error("ToutiaoInterface error!",[
                'msg' => json_encode($content, JSON_UNESCAPED_UNICODE),
                'url' => $content['code'] === 40100?$base_url:$url]);
            return $content;
        }

        return $content['data'];
    }

    public function verificationRequestUri($method, $parameters, $url)
    {
        $httpBasicInfo = self::$defaults["http"];

        $url = $httpBasicInfo["base_uri"] . "/" . $url;

        if (strtoupper($method) === "GET") {
            $url .= "?" . http_build_query($parameters);
        }

        return $url;
    }

    public function verificationRequestParameters($method, $parameters)
    {

        if($this->app['config']->get('access_token')){
            $token = $this->app['config']->get('access_token');
        }elseif (isset($parameters['json']['access_token'])){
            $token = $parameters['json']['access_token'];
        }else{
            $token = $parameters['access_token'];
        }
        self::$defaults["headers"]['Access-Token'] = $token;

        $debug = $this->app['config']->get('debug');

        if ($debug) {
            self::$defaults["headers"]['X-Debug-Mode'] = 1;
        }

        //如果是post, 确保 GuzzleHttp 请求参数
        if ($method === "POST" && !array_key_exists('json',$parameters)) {
            $parameters = ["json" => $parameters];
        }

        return array_merge(self::$defaults, $parameters);
    }

    /**
     * 多账号使用
     * 传入广告主id
     * @param array $owner
     */
    public function setDefaultConfig($owner)
    {
        $this->app['config']->access_token = $owner["access_token"];
        $this->app['config']->advertiser_id = $owner["advertiser_id"];
    }

    public function validateRequiredParams(array $data, array $keys)
    {
        if (!Arr::keys_all_exists($data, $keys)) throw new ValidateRequestParamException('required keys :' . implode(',', $keys));
    }

}