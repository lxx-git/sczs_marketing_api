<?php


namespace Sczs\MarketingApi\Toutiao\Kernel\Interfaces;


interface AccessTokenInterface
{
    public function getToken();
}