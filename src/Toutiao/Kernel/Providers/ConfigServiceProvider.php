<?php


namespace Sczs\MarketingApi\Toutiao\Kernel\Providers;


use Sczs\MarketingApi\Toutiao\Kernel\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ConfigServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['config']) && $pimple['config'] = function ($app){
            return new Config($app->getConfig());
        };
    }
}