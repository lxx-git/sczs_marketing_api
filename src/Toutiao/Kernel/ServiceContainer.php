<?php


namespace Sczs\MarketingApi\Toutiao\Kernel;
use Sczs\MarketingApi\Toutiao\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Toutiao\Kernel\Providers\ConfigServiceProvider;
use Sczs\MarketingApi\Toutiao\Kernel\Providers\HttpServiceProvider;
use Pimple\Container;

class ServiceContainer extends Container
{
    protected $config = [];

    protected $defaultConfig = [
        'token_num' => 1
    ];

    protected $providers = [];

    public function __construct($config = [],array $values = [])
    {
        $this->config = $config;
        parent::__construct($values);
        $this->registerProviders($this->getProviders());
    }

    public function getConfig() : array
    {

        $base = [
            'http' => [
                'timeout' => 60,
                'base_uri' =>  'https://ad.oceanengine.com/',
            ]
        ];

        return array_replace_recursive($base,$this->defaultConfig,$this->config);
    }


    public function getProviders() : array
    {
        return array_merge([
            ConfigServiceProvider::class,
            HttpServiceProvider::class,
//            CacheServiceProvider::class,
        ],$this->providers);
    }

    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}