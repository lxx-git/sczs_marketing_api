<?php


namespace Sczs\MarketingApi\Toutiao\Material;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Image extends BaseHttpClient
{
    /**
     * 上传广告图片
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function upload(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'upload_type'
        ]);
        if ( $data['upload_type'] == 'UPLOAD_BY_FILE' ){
            $this->validateRequiredParams($data,[
                'image_file',
                'image_signature'
            ]);
        }elseif($data['upload_type'] == 'UPLOAD_BY_URL'){
            $this->validateRequiredParams($data,[
                'image_url',
            ]);
        }
        return $this->multipartRequest('open_api/2/file/image/ad/',$data);
    }

    /**
     * 获取图片素材
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function list(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);

        return $this->httpJsonGet('open_api/2/file/image/get/',$data);
    }

    /**
     * 获取同主体下广告主图片素材
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function advertiserImage(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'image_ids'
        ]);
        return $this->httpJsonGet('open_api/2/file/image/ad/get/',$data);
    }
}