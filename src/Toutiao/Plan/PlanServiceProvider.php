<?php


namespace Sczs\MarketingApi\Toutiao\Plan;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class PlanServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['plan']) && $pimple['plan'] = function ($app){
            return new Plan($app);
        };
    }
}