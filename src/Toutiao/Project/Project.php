<?php


namespace Sczs\MarketingApi\Toutiao\Project;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Project  extends BaseHttpClient
{
    /**
     * 获取项目列表
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonGet('open_api/v3.0/project/list/',$data);
    }

    /**
     * 创建项目
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'landing_type',
            'app_promotion_type',
            'marketing_goal',
            'ad_type',
            'name',
        ]);

        return $this->httpJsonPost('open_api/v3.0/project/create/',$data);
    }

    /**
     * 更新项目
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'project_id',
        ]);

        return $this->httpJsonPost('open_api/v3.0/project/update/',$data);
    }

    /**
     * 批量更新项目状态
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateStatus(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data'
        ]);

        return $this->httpJsonPost('open_api/v3.0/project/status/update/',$data);
    }

    /**
     * 批量删除项目
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'project_ids'
        ]);

        return $this->httpJsonPost('open_api/v3.0/project/delete/',$data);
    }

    /**
     * 批量更新项目预算
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBudget(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data'
        ]);

        return $this->httpJsonPost('open_api/v3.0/project/budget/update/',$data);
    }
}