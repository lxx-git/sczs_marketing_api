<?php


namespace Sczs\MarketingApi\Toutiao\Project;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class ProjectServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['project']) && $pimple['project'] = function ($app){
            return new Project($app);
        };
    }
}