<?php


namespace Sczs\MarketingApi\Toutiao\Promotion;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Promotion  extends BaseHttpClient
{
    /**
     * 获取广告列表
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonGet('open_api/v3.0/promotion/list/',$data);
    }

    /**
     * 创建广告
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'project_id',
            'name',
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/create/',$data);
    }

    /**
     * 修改广告
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'promotion_id',
            'name',
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/update/',$data);
    }

    /**
     * 批量更新广告预算
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBudget(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data',
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/budget/update/',$data);
    }

    /**
     * 批量更新广告出价
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBid(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data',
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/bid/update/',$data);
    }

    /**
     * 批量更新广告启用状态
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateStatus(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data'
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/status/update/',$data);
    }

    /**
     * 批量删除广告
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'promotion_ids'
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/delete/',$data);
    }

    /**
     * 批量获取广告审核建议
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function rejectReason(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'promotion_ids'
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/reject_reason/get/',$data);
    }

    /**
     * 批量更新广告素材启用状态
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateMaterialStatus(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'promotion_id',
            'data'
        ]);

        return $this->httpJsonPost('open_api/v3.0/material/status/update/',$data);
    }

    /**
     * 批量修改深度出价
     * @param array $data
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateDeepBid(array $data)
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'data',
        ]);

        return $this->httpJsonPost('open_api/v3.0/promotion/deepbid/update/',$data);
    }
}