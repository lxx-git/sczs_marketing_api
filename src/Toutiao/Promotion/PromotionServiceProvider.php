<?php


namespace Sczs\MarketingApi\Toutiao\Promotion;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class PromotionServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['promotion']) && $pimple['promotion'] = function ($app){
            return new Promotion($app);
        };
    }
}