<?php


namespace Sczs\MarketingApi\Toutiao\Report;


use Sczs\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{

    /**
     * 广告主数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function advertiser(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date'
        ]);
        return $this->request("GET", 'open_api/2/report/advertiser/get/', $data);
    }

    /**
     * 广告组数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function campaign(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date'
        ]);
        return $this->request("GET", 'open_api/2/report/campaign/get/', $data);
    }

    /**
     * 获取广告计划数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function plan(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date'
        ]);
        return $this->request("GET", 'open_api/2/report/ad/get/', $data);
    }

    /**
     * 获取创意数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function creative(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date'
        ]);
        return $this->request("GET", 'open_api/2/report/creative/get/', $data);
    }

    /**
     * 分级模糊数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function misty(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date',
            'group_by'
        ]);
        return $this->request("GET", 'open_api/2/report/misty/get/', $data);
    }

    /**
     * 视频素材数据
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function video(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date'
        ]);
        return $this->request("GET", 'open_api/2/report/video/get/', $data);
    }

    /**
     * 多合一报表接口
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function integrated(array $data): array
    {
        $this->validateRequiredParams($data, [
            'start_date',
            'end_date',
            'group_by'
        ]);
        return $this->request("GET", 'open_api/2/report/integrated/get/', $data);
    }


    ////////////////////////////////////巨量引擎体验版 广告数据报表///////////////////////////////////////////
    /**
     * 项目数据报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function project(array $data):array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
            'start_date',
            'end_date',
            'fields'
        ]);

        return $this->request("GET", 'open_api/v3.0/report/project/get/', $data);
    }

    /**
     * 广告数据报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function promotion(array $data):array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
            'start_date',
            'end_date',
            'fields'
        ]);

        return $this->request("GET", 'open_api/v3.0/report/promotion/get/', $data);
    }


    /**
     * 素材数据报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     * @param array $data
     * @return array
     */
    public function material(array $data):array
    {
        $this->validateRequiredParams($data, [
            'advertiser_id',
            'start_date',
            'end_date',
            'fields'
        ]);

        return $this->request("GET", 'open_api/v3.0/report/material/get/', $data);
    }


    /**
     * 自定义报表
     * @param array $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function custom(array $data){
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'dimensions',
            'metrics',
            'filters',
        ]);

        return $this->request('GET',"open_api/v3.0/report/custom/get/",$data);
    }


}