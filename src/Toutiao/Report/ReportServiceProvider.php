<?php


namespace Sczs\MarketingApi\Toutiao\Report;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class ReportServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['report']) && $pimple['report'] = function ($app){
            return new Report($app);
        };
    }
}