<?php

namespace Sczs\MarketingApi\Vivo\Advertiser;

use Sczs\MarketingApi\Vivo\Kernel\Http\BaseHttpClient;

class Advertiser extends BaseHttpClient
{

    /**
     * vivo账号
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return array|string
     */
    public function advertiser_query(array $parameters = [])
    {
        return $this->request("POST", "account/advertiser/query", $parameters);
    }
}
