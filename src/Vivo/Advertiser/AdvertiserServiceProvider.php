<?php

namespace Sczs\MarketingApi\Vivo\Advertiser;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class AdvertiserServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["advertiser"] = function ($app) {
            return new Advertiser($app);
        };
    }
}