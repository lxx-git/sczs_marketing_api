<?php

namespace Sczs\MarketingApi\Vivo;

use Sczs\MarketingApi\Vivo\Advertiser\AdvertiserServiceProvider;
use Sczs\MarketingApi\Vivo\Kernel\Cache\CacheServiceProvider;
use Sczs\MarketingApi\Vivo\Kernel\ServiceContainer;
use Sczs\MarketingApi\Vivo\Oauth\OauthServiceProvider;
use Sczs\MarketingApi\Vivo\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Sczs\MarketingApi\Vivo\Report\Report $report
 * @property \Sczs\MarketingApi\Vivo\Advertiser\Advertiser $advertiser
 * @property \Sczs\MarketingApi\Vivo\Oauth\Oauth $oauth
 * @property \Sczs\MarketingApi\Vivo\Kernel\Cache\Cache $cache
 *
 * @package Sczs\MarketingApi\Vivo
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        ReportServiceProvider::class,
        CacheServiceProvider::class,
        AdvertiserServiceProvider::class,
        OauthServiceProvider::class,
    ];
}
