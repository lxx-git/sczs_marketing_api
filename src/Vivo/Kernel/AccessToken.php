<?php

namespace Sczs\MarketingApi\Vivo\Kernel;

use Sczs\MarketingApi\Vivo\Kernel\Interfaces\AccessTokenInterface;
use Sczs\MarketingApi\Vivo\Kernel\Traits\HttpRequest;

class AccessToken implements AccessTokenInterface
{
    use HttpRequest {
        request as VivoRequest;
    }

    protected $app;

    protected $oauth2TokenUrl = "https://marketing-api.vivo.com.cn/openapi/v1/oauth2/token";

    protected $authorizeCodeKey = "sczs:vivo:authorize_code";

    protected $authorizeAccessTokenKey = "sczs:vivo:access_token_";

    protected $authorizeRefreshTokenKey = "sczs:vivo:refresh_token_";

    protected $managerId = "";


    //authorization_code,state
    //client_id,client_secret,grant_type,authorization_code
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @throws Exception\VivoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $oauthClient
     * @return mixed
     */
    public function oauth2Token($oauthClient)
    {
        $parameters = [
            'client_id' => $oauthClient["client_id"],
            'grant_type' => $oauthClient["grant_type"],
            'client_secret' => $oauthClient["client_secret"],
            'code' => $oauthClient["authorization_code"],
        ];

        $this->managerId = $oauthClient["manager_id"];

        $response = $this->VivoRequest("GET", $this->oauth2TokenUrl, ["query" => $parameters]);

        $this->setAccessToken($response);

        return $response;
    }


    public function refreshToken()
    {
        // TODO: Implement refreshToken() method.
    }


    public function setAccessToken($data)
    {
        $this->getCacheClient()->hmSet(
            $this->authorizeAccessTokenKey . $this->managerId, $data
        );
        $this->getCacheClient()->setExp($this->authorizeAccessTokenKey . $this->managerId, 86400 * 359);
    }

    public function access_token($managerId)
    {
        return $this->getCacheClient()->hgetall(
            $this->authorizeAccessTokenKey . $managerId
        );
    }

    /**
     * @return \Sczs\MarketingApi\Vivo\Kernel\Cache\Cache
     */
    public function getCacheClient()
    {
        return $this->app['cache'];
    }

    public function getToken()
    {

    }
}