<?php

namespace Sczs\MarketingApi\Vivo\Kernel\Http;

use Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException;
use Sczs\MarketingApi\Vivo\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as VivoRequest;
    }

    /**
     * @var \Sczs\MarketingApi\Vivo\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://marketing-api.vivo.com.cn/openapi/v1/'
        ]
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @throws VivoException
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        if(isset($app->defaultConfig["account_id"])) {
            $this->manager($app->defaultConfig["account_id"]);
        }else{
            $this->commonParameters($app->defaultConfig);
        }
    }

    /**
     * @throws VivoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        //获取请求路由附加参数,vivo验证在路由里
        $url = $this->verificationRequestUri($url);

        return $this->VivoRequest($method, $url, ["json" => $parameters]);
    }

    /**
     * 验证请求类型 获取请求路由参数
     * get  基础参数需要与请求参数合并
     * post 只需要获取基础参数
     * @param $url
     * @return string
     */
    public function verificationRequestUri($url)
    {
        $httpBasicInfo = $this->defaults["http"];

        $url = $httpBasicInfo["base_uri"] . $url;

        $url .= "?" . http_build_query($this->commonParameters);

        return $url;
    }

    /**
     * 多账号使用
     * 传入广告主id
     * @param array $owner
     */
    public function setDefaultConfig($owner)
    {
        $this->owner["advertiser_id"] = $owner["advertiser_id"];
        $this->owner["access_token"] = $owner["access_token"];

        $this->commonParameters($this->owner);
    }

    /**
     * @throws VivoException
     * @param $account_id
     */
    public function manager($account_id)
    {
        $token = $this->app->oauth->access_token($account_id);
        if(!$token) throw new VivoException("没有代理账号access_token");
        $this->commonParameters($token);
    }

    /**
     * 组合请求路由的基础参数
     * @param $owner
     */
    public function commonParameters($owner)
    {
        $this->commonParameters = array_merge(
            [
                "nonce" => md5(uniqid("zs_lee_vivo", true)),
                "timestamp" => intval(microtime(true) * 1000)
            ],
            $owner
        );
    }
}