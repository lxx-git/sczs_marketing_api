<?php

namespace Sczs\MarketingApi\Vivo\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();

    public function refreshToken();
}