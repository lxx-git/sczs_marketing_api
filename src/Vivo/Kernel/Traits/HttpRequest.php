<?php

namespace Sczs\MarketingApi\Vivo\Kernel\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException;

/**
 * Trait HttpRequest
 *
 * @package Sczs\MarketingApi\Tencent\Traits
 */
trait HttpRequest
{

    /**
     * @throws GuzzleException
     * @throws VivoException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function request(string $method, string $url, array $parameters = [])
    {

        $ret = $this->httpClient()->request(
            $method,
            $url,
            $parameters
        );

        if(!$ret) throw new GuzzleException(json_encode(["msg" => "Vivo ads returned an error:", "code" => "-1"]));

        $content = json_decode($ret->getBody()->getContents(), 320);

        if (!$content) throw new VivoException(json_encode(["msg" => "Vivo ads return empty content", "code" => "-1"]));

        if ($content["code"] !== 0) throw new VivoException(json_encode($content, 320));

        if (isset($content["data"]["list"]))
            return $content["data"]["list"];
        else
            return $content["data"];

    }

    /**
     * 实例化请求
     * @return Client
     */
    public function httpClient()
    {
        return new Client();
    }
}
