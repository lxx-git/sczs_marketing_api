<?php

namespace Sczs\MarketingApi\Vivo\Oauth;

use Sczs\MarketingApi\Vivo\Kernel\AccessToken;
use Sczs\MarketingApi\Vivo\Kernel\ServiceContainer;

class Oauth extends AccessToken
{
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
    }
}