<?php

namespace Sczs\MarketingApi\Vivo\Report;

use Sczs\MarketingApi\Vivo\Kernel\Http\BaseHttpClient;

class Report extends BaseHttpClient
{

    /**
     * vivo数据报表
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return array|string
     */
    public function daily_reports(array $parameters = [])
    {
        return $this->request("POST", "adstatement/summary/query", $parameters);
    }

    /**
     * vivo广告计划
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return array|string
     */
    public function campaign(array $parameters = [])
    {
        return $this->request("POST", "ad/campaign/pageInfo", $parameters);
    }

    /**
     * vivo广告组
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function group(array $parameters = [])
    {
        return $this->request("POST", "ad/group/pageInfo", $parameters);
    }

    /**
     * vivo广告
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Sczs\MarketingApi\Vivo\Kernel\Exception\VivoException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function advertisement(array $parameters = [])
    {
        return $this->request("POST", "ad/advertisement/pageInfo", $parameters);
    }
}