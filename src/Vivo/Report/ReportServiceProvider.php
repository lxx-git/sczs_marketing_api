<?php

namespace Sczs\MarketingApi\Vivo\Report;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ReportServiceProvider
 * @package Sczs\MarketingApi\Tencent\Report
 */
class ReportServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple["report"] = function ($app) {
            return new Report($app);
        };
    }
}